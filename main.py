from machine import I2C, Pin
from ssd1306 import  SSD1306_I2C
from dht import DHT22
from time import time, sleep
from urequests import get

i2c = I2C(scl=Pin(5), sda=Pin(4))

oled_conn = SSD1306_I2C(width=64, height=48, i2c=i2c,  addr=0x3c)

dht_conn = DHT22(Pin(2))

def ez_text(fb):
    oled_conn.fill(0)
    if type(fb) == str:
        fb = chunk(fb[:48], 8)
    for text, row in zip(fb, range(0, 48, 8)):
        oled_conn.text("{: ^8.8}".format(str(text)), 0, row)
    oled_conn.show()

def measure_realtime():
    dht_conn.measure()
    sleep(0.2)
    dht_conn.measure()

trigger_time = time()-(29*60)

while True:
    warning = False
    measure_realtime()
    if dht_conn.temperature() > 30:
        warning = True
        if (time() - trigger_time) < (30*60):
            get("http://maker.ifttt.com/trigger/sicaklik_nem/with/key/bakAFpPW1JxIJtFwS8EWS3", json={"value1":dht_conn.temperature(), "value2":dht_conn.humidity()}, headers={'Content-Type': 'application/json'})
            tirgger_time = time()
    if warning:
        title = "CokSicak"
    else:
        title = "Normal"
    ez_text([title, "-"*8,"Temp :  ", "{:03.1f} C".format(dht_conn.temperature()), "Humid:  ", "{:03.1f} %".format(dht_conn.humidity())])
    sleep(10)